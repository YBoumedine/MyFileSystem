# Travail pratique 1

## Description

Ce projet à été conçu dans le cadre du cours de système d'exploitation INF3172 à l'UQÀM. Le logiciel se résume à créer une simulation d’un système de fichiers. Pour se faire on crée un fichier de 500 Ko qui simule le disque d'un ordinateur. Il stocke des chaînes de caractères qui joue le rôle des fichiers gérer par le système.


## Auteurs

- Youva Boumedine (BOUY19020606
- Hugues Arly Chanoine (CHAH26108905)

## Plateformes supportées

les plateformes suivantes sont supportées et testées par notre projet:

	*MacOS 10.12.1 Sierra
	*Linux Ubuntu 16.04 LTS

## Fonctionnement

Pour utiliser notre programme, il vous suffit de télécharger notre fichier via Gitlab ou de Moodle, le mettre sur votre home et d’écrire dans votre terminal :

	cd MyFileSystem

## Installation

Pour rendre le projet fonctionnel il vous suffit de suivre les étapes suivantes toutes en restant dans le repertoire MyFileSystem:

	 make
Ensuite, Pour nettoyer le dossier:

	make clean


-----------------------

######Premier Exemple:


bin/fs frag1.test

-----------------------
######Deuxième Exemple:

bin/fs frag2.test

-----------------------

## contenu du TP

	- MyFileSystem
		- src
			- blocks
			- blocks.h
			- files
			- fs.c
			- nodes.c
			- nodes.h
			- repo.c
			- repo.h
		- MakeFile
		- README.md


## Références

Voici les sites que nous avons utilisés pour faire le projet :

	 http://stackoverflow.com/
	 https://www.tutorialspoint.com/
	 http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html



## Statut


##### Malgré que le programme semble fonctionner sur linux et sur malt à partir d’un ordinateur avec linux correctement. Le programme ne semble pas vouloir fonctionner correctement sur mac ni sur malt à partir d’un iOS parfaitement. Sur iOS, nous obtenons souvent un « abort trap 6 » après quelques commandes. Le problème est sans doute relié à de l’allocation de mémoire. Nous avons utilisé Valgrind et nous avons retiré le plus d’erreurs possibles mais le problème persiste toujours sur le iOS.


##### En conclusion, puisque la fonction qui supprimer le répertoire est absente et la fonction créer le répertoire ne fonctionnes pas entièrement, le projet est incomplet, la sauvegarde des repertoire également est incomplet. Le reste, par contre, fonctionne correctement. Créer répertoire fonctionne correctement tous seule mais lorsque combiné avec créer fichier sa cause des problèmes. Par exemple, lorsqu’on fait « /main/a/a2.txt  se String ne devrait pas fonctionner », et que le répertoire « a » n’existe pas, le fichier est tous de même crée.
