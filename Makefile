CC = gcc
SOURCEDIR = src
BINDIR = bin
CFLAGS = -Wall
SRC = $(wildcard $(SOURCEDIR)/*.c)
OBJ = $(patsubst $(SOURCEDIR)/%.c,%.o, $(SRC))
EXEC = fs
MKDIR_P = mkdir -p



$(BINDIR)/$(EXEC) : $(OBJ)
	$(MKDIR_P) $(@D)
	$(CC) $(OBJ) $(LFLAGS) -o $@

$(OBJ) : $(SRC)
	$(CC) $(CFLAGS) -c -g $(SRC)  $<





.PHONY : clean

clean:
	rm -f $(OBJ) $(BINDIR)/$(EXEC) *.bin *.txt
