#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "repo.h"
#include "nodes.h"
#include "blocks.h"


static int repoCounter = 0;
static char** arrayRepo;

bool searchRepoFile(char *pathName){
  char *repoPath, *temp, *token, **array;
  int cout = 1, i=0, j =0, sizeArr=0, tempSize=0;

  array = malloc(sizeof(char *));
  repoPath = strdup(pathName);
  token = strtok(repoPath, "/");

  while (token != NULL){
    array = realloc(array, cout*sizeof(char *));
    array[cout -1] = malloc(strlen(token));
    if(array[cout-1] == NULL){
      fprintf(stderr, "Error malloc hihi xd\n");
      exit(1);
    }
    strcpy(array[cout-1], token);
    sizeArr += sizeof(array[cout-1]);
    cout ++;
    token = strtok(NULL, "/");
  }


  temp = malloc(sizeArr * sizeof(char));
  tempSize = sizeof(temp);
  memset(temp, '\0', tempSize);

  while(i<cout-2){
    strcat(temp, "/");
    strcat(temp, array[i]);
    i++;
  }

  if(searchRepo(temp) == 0 && strlen(temp) != 0){
    while(j<cout-1){
      free(array[j]);
      j++;
    }
   free(array);
    free(temp);
    free(repoPath);
    return false;

  }else{
    while(j<cout-1){
      free(array[j]);
      j++;
    }
    free(array);
    free(temp);
    free(repoPath);
    return true;
  }

}

int searchRepo(char *pathName){
  char *repoPath, *temp, *token;
  int i, a = 0, tokenSize, tempSize;

  repoPath = strdup(pathName);
  token = strtok(repoPath, "/");
  temp = malloc(sizeof(token)+1);
  tempSize = sizeof(temp);
  memset(temp, '\0', tempSize);

  while(token != NULL){

    strncat(temp, "/", 1);
    tokenSize = strlen(token);
    strncat(temp, token, tokenSize);


    for(i=0; i<repoCounter; i++){
      if(strcmp(arrayRepo[i], temp) == 0){
        a ++;
        break;
      }else{
        a = 0;
      }
    }
    token = strtok(NULL, "/");

    if(token != NULL && a == 0){
      a = 5;
      free(repoPath);
      free(temp);
      return a;
    }
    temp = realloc(temp, sizeof(temp) + sizeof(token));
  }

  if(token == NULL && a != 0 ){
    free(repoPath);
    free(temp);
    return a;
  }
  a = 0;
  free(repoPath);
  free(temp);
  return a;
}


void createRepo(char *pathName){
  char *truePath;
  int verif = 0;

  truePath = strdup(pathName);

  if(repoCounter != 0){
    verif = searchRepo(truePath);
  }


  if(verif == 0){
    repoCounter++;
    if(repoCounter == 1){
      arrayRepo = malloc(sizeof(char *));
    }else{
      arrayRepo = realloc(arrayRepo, repoCounter*sizeof(char*));
    }
    arrayRepo[repoCounter-1] = strdup(truePath);
  }  else if(verif == 5){
    fprintf(stderr, "Parent repertory does not exist..Continuing.\n");
  }else{
    fprintf(stderr, "Repertory already exists.. Continuing.\n");
  }

  free(truePath);

}


void deleteRepo(){
}

void printRepo(){
  int i;

  for (i=0; i<repoCounter; i++){
    fprintf(stderr, "%s\n", arrayRepo[i]);
  }
}

void saveRepoFile ( FILE *repoFile){
  int i;

  for (i = 0; i< repoCounter; i++){
    int length = strlen(arrayRepo[i]);
    fwrite(&length, sizeof(int), 1, repoFile);
    fwrite(arrayRepo[i], length, 1, repoFile);
    free(arrayRepo[i]);
  }
  free(arrayRepo);
}

void loadRepoFile (FILE *repoFile){
  char c, *repoPath;
  int length;

  c = fgetc(repoFile);

  while(c != EOF){
    repoCounter++;
    fseek(repoFile, -1, SEEK_CUR);
    fread(&length, sizeof(int), 1, repoFile);
    repoPath = malloc(length);
    fread(repoPath, sizeof(char), length, repoFile);

    if(repoCounter == 1){
      arrayRepo = malloc(sizeof(char *));
    }else{
      arrayRepo = realloc(arrayRepo, sizeof(char *) * repoCounter);
    }

    arrayRepo[repoCounter - 1] = strdup(repoPath);

    c = fgetc(repoFile);
  }

  fclose(repoFile);

  free(repoPath);
}
