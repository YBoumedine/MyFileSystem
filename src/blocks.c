#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "blocks.h"

#define BLOCK_SIZE 16
static int table [1000];
static char *arrayBlocks[32000];



// ------------- Methods for manipulating the array ------------- //

bool arrayFull(){
  int i;
  for (i = 0; i < 32000; i++) {
    testerBit(i);
    if(!testerBit(i)){
      return false;
    }
  }
  return true;
}

char *readArray(int i){
  return arrayBlocks[i];
}


void writeArray(FILE *FileSystem){
  int i;
  for(i=0; i<32000; i++){
    if(testerBit(i)){
      fwrite(arrayBlocks[i], 16, 1, FileSystem);
    }
  }
}

void freeArrayBlock(int i){
  libererBit(i);
  free(arrayBlocks[i]);
}

int writeInBlock(char *block){
  int i;

  for(i=0; i<32000; i++){
    if(!testerBit(i)){
      arrayBlocks[i] = malloc(16);
      memset(arrayBlocks[i], '\0', 16);
      strcpy(arrayBlocks[i], block);
      OccuperBit(i);
      break;
    }
  }

  return i;
}


// ------------- Methods for the table of bits ------------- //

void OccuperBit(int pos){
  int i = pos/32;
  int thePos = pos%32;

  unsigned int flag = 1;

  flag = flag << thePos;

  table[i] = table[i] | flag;
}

void libererBit(int pos){
  int i = pos/32;
  int thePos = pos%32;

  unsigned int flag = 1;

  flag = flag << thePos;
  flag = ~flag;
  table[i] = table[i] & flag;

}

bool testerBit(int pos){
  int i = pos/32;
  int thePos = pos%32;

  unsigned int flag = 1;

  flag = flag << thePos;

  if(table[i] & flag){
    return true;
  }else{
    return false;
  }
}

// ------------- Save the blocks inside a file ------------- //



void saveBitsState(FILE *bitsFile){

  if(bitsFile == NULL){
    fprintf(stderr, "error loading file.. Exiting.");
    exit(1);
  }

  fwrite (table, sizeof(int), 1000, bitsFile);
  fclose(bitsFile);

}

void loadBlocks(FILE *blocksFile, FILE *bitsFile){
  int i = 0;
  char *content;

  fread(table, sizeof(int), 1000, bitsFile);

  while(testerBit(i)){
    content = malloc(BLOCK_SIZE);
    fread(content, BLOCK_SIZE, 1, blocksFile);
    arrayBlocks[i] = strdup(content);
    i++;
  }
  free(content);
}
