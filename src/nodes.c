#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "nodes.h"
#include "blocks.h"

static int fileCounter = 0;
static struct iNode **arrayFiles;

// ------------- Load the nodes inside the lists ------------- //


void loadNodes(FILE *iNodeFile){
  int length;
  char *content, c;
  c = fgetc(iNodeFile);

  while(c != EOF){
    fileCounter++;
    fseek(iNodeFile, -1, SEEK_CUR);
    fread(&length, sizeof(int), 1, iNodeFile);
    content = malloc(length*sizeof(char));
    fread(content, sizeof(char), length, iNodeFile);

    if(fileCounter == 1){
      arrayFiles = malloc(sizeof(struct iNode *));
    }else{
      arrayFiles = realloc(arrayFiles, sizeof(struct iNode *) * fileCounter);
    }

    arrayFiles[fileCounter-1] = malloc(sizeof(struct iNode));

    arrayFiles[fileCounter - 1]->pathName = strdup(content);

    fread(arrayFiles[fileCounter-1]->blocksNum, sizeof(arrayFiles[fileCounter-1]->blocksNum), 1, iNodeFile);

    c = fgetc(iNodeFile);
  }
  fclose(iNodeFile);
  free(content);
}


// ------------- Save the nodes inside the lists ------------- //

void saveNodesInArray(struct iNode *fileNode){
  fileCounter++;
  if (fileCounter == 1){
    arrayFiles = calloc(1, sizeof(fileNode));
    arrayFiles[fileCounter-1] = fileNode;
  }else{
     arrayFiles = realloc(arrayFiles, sizeof(fileNode)*fileCounter);
     arrayFiles[fileCounter-1] = fileNode;
  }
}


// ------------- Save the nodes inside a file ------------- //
void saveFileNodeState(FILE *iNodeFile){
  int i=0;

  for(i = 0; i<fileCounter; i++){
    writeNode(arrayFiles[i], iNodeFile);
  }
  fclose(iNodeFile);
}
// ------------- Save the parameters inside a file ------------- //


void writeNode(struct iNode *node, FILE *iNodeFile){

  int length = strlen(node->pathName);
  fwrite(&length, sizeof(int), 1, iNodeFile);
  fwrite(node->pathName, length, 1, iNodeFile);
  fwrite(node->blocksNum, sizeof(int), 16, iNodeFile);
  free(node->pathName);
  free(node);
}

// ------------- Search file in the nodes ------------- //

int searchNode(char *pathName){
  int i;

  for(i=0; i<fileCounter; i++){
    if(strcmp(arrayFiles[i]->pathName, pathName) == 0){
      return i;
    }
  }

  return -1;
}

void readNode(char *pathName){
  int i, j;

    j = searchNode(pathName);

    if(j != -1){
    for(i=0; i<16;i++){
      if(arrayFiles[j]->blocksNum[i] != -1){
        printf("%s",readArray(arrayFiles[j]->blocksNum[i]));
      }
    }
    printf("\n");
  }else{
    fprintf(stderr, "The file does not exist.. Continuing.\n");
  }
}

void deleteNode(char *pathName){
  int i, j;

  j = searchNode(pathName);

  if(j != -1){
    free(arrayFiles[j]->pathName);
    for(i=0; i<16; i++){
      if(arrayFiles[j]->blocksNum[i] != -1){
        freeArrayBlock(arrayFiles[j]->blocksNum[i]);
      }
    }

    free(arrayFiles[j]);

    if(arrayFiles[j] != arrayFiles[fileCounter-1]){
      arrayFiles[j] = arrayFiles[fileCounter-1];
    }

    fileCounter--;

  }else{
    fprintf(stderr, "The file does not exist.. Continuing.\n");
  }
}
