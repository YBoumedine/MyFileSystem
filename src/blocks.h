#ifndef BLOCKS_H
#define BLOCKS_H

#include<stdbool.h>

/**
 *
 * This function is use to read the array of blocks.
 *
 *  @param i       the index of the block
 */
char *readArray(int i);

/**
 *
 * This function is use to write in the array.
 *
 *  @param FileSystem      content of FileSystem
 */
void writeArray(FILE *FileSystem);

/**
 *
 * This function is use to write in the blocks.
 *
 *
 *  @param block      content of the block
 */
int  writeInBlock(char *block);

/**
 *
 * This function is use to create files.
 *
 *
 *  @param i        the index of the block
 */
void freeArrayBlock(int i);


/**
 *
 * This function puts 1 in the bit that being use.
 *
 *
 * @param pos        the index of the bit
 */
void OccuperBit(int pos);

/**
 *
 * This function is to free the bit, putting 0 where you want it free.
 *
 *
 *  @param pos        the index of the bit
 */
void libererBit(int pos);

/**
 *
 * This function verifies if the bit is complete.
 *
 *  @param pos        the index of the bit
 */
bool testerBit(int pos);

/**
 *
 * This function saves the state of the blocks.
 *
 *
 */
void saveBlocksState();

/**
 *
 * This function saves the state of the bits.
 *
 *
 */
void saveBitsState();

/**
 *
 * This function load the required blocks.
 *
 * @param blocksFile        content of blocksFile
 * @param bitsFile          content of bitsFile
 */
void loadBlocks(FILE *blocksFile, FILE *bitsFile);

/**
 *
 * This function indicate if the array is full or not.
 * Return's a boolean.
 *
 */
bool arrayFull();

#endif
