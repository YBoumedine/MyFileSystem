#ifndef REPO_H
#define REPO_H

/**
 *
 * Search if the repertory existe or/and is already created.
 * Returns a int to indicate if its present or not.
 *
 */
int searchRepo();

/**
 *
 * This function creates a repertory.
 *
 * @param pathName        the address of the file
 */
void createRepo(char *pathName);

/**
 *
 * This function deletes the repertory if it exists.
 *
 */
void deleteRepo();

/**
 *
 * This Function prints the repertory.
 *
 */
void printRepo();

/**
 *
 *  This function search if the repertory in the pathName of a file exist.
 *
 * @param pathName        the address of the file
 */
bool searchRepoFile(char *pathName);

/**
 *
 * This function save all the repertory in a binary file.
 *
 * @param repoFile       is a pointer to the binary file
 */
void saveRepoFile(FILE *repoFile);

/**
 *
 * This function loads the repertory.
 *
 * @param repoFile      is a pointer to the binary file
 */
void loadRepoFile(FILE *repoFile);

#endif
