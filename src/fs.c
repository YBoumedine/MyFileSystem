#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include<stdbool.h>
#include "blocks.h"
#include "nodes.h"
#include "files.h"
#include "repo.h"

#define BUFFER_SIZE 500



int main(int argc, char *argv[]){
  char *command, *path, *content, str[BUFFER_SIZE], delim[2] = " ";
  FILE *param = fopen(argv[1], "r");

  printf("WARNING : The functions delete repertory, the save file of repertories and the verification of parent folders of files do not work.\n\n");





  // Opening the necessary files for the system.
  
  FILE *myFileSystem = fopen("FileSystem.txt", "r+");
  FILE *bitsFile = fopen("bitsTable.bin", "r+b");
  FILE *iNodeFile = fopen("nodeFile.bin", "r+b");

  if(myFileSystem == NULL){
    myFileSystem = fopen("FileSystem.txt", "w");
  }

  if(bitsFile == NULL){
    bitsFile = fopen ("bitsTable.bin", "w+b");
  }else{
    loadBlocks(myFileSystem, bitsFile);
  }

  if(iNodeFile == NULL){
    iNodeFile = fopen("nodeFile.bin", "w+b");
  }else{
    loadNodes(iNodeFile);
  }

  // Verifies if there is no older state of repositories, else loads them.

  // if(repoFile == NULL){
  //   repoFile = fopen("repoFile.bin", "w+b");
  // }else{
  //   loadRepoFile(repoFile);
  // }

  bitsFile = fopen("bitsTable.bin", "wb");
  iNodeFile = fopen("nodeFile.bin", "wb");
  // repoFile = fopen("repoFile.bin", "wb"); Re opens the file to rewrite the new state of repositories at end of program.




  while(fgets(str,BUFFER_SIZE, param) != NULL){

    command = strtok(str, delim);



    if(feof(param)){
      break;
    }

    if(strcmp(command, "creation_fichier") == 0){
      path = strtok(NULL, delim);
      content = strtok(NULL,"\n");

      printf("%s %s %s \n", command, path, content);

      if(strlen(path) > 40){
        fprintf(stderr, "Path name is too long.. Continuing.\n");
      }else if(strlen(content) > 128){
        fprintf(stderr,"The file content is too big.. Skipping instruction.\n");
      }else if(content == NULL){
        fprintf(stderr, "The content is empty.. Exiting.\n");
      }else{
        if(!arrayFull()){
          if(searchNode(path) == -1){
            creation_fichier(path, content);
          }else{
            fprintf(stderr, "The file already exists.. Continuing.\n");
          }
        }else{
          fprintf(stderr, "The disk is full.. Continuing.\n");
        }
      }
    }

     if(strcmp(command,"suppression_fichier")== 0){
       path = strtok(NULL, "\n");
       if(strlen(path) > 40){
         fprintf(stderr, "Path name is too long.. Continuing.\n");
       }
       printf("%s %s \n", command, path);
       deleteNode(path);
     }




     if(strcmp(command,"lire_fichier") == 0){
       path = strtok(NULL, "\n");
       if(strlen(path) > 40){
         fprintf(stderr, "Path name is too long.. Continuing.\n");
       }
       printf("%s %s \n", command, path);
       readNode(path);
    }




     if(strcmp(command, "creation_repertoire") == 0){
       path = strtok(NULL, "\n");
       printf("%s %s \n", command, path);
       createRepo(path);
     }



     if(strcmp(command, "suppression_repertoire") == 0){
       fprintf(stderr, "This method was not implemented.\n");
    }
  }

    myFileSystem = fopen("FileSystem.txt", "w");
    writeArray(myFileSystem);
    fclose(myFileSystem);

  saveBitsState(bitsFile);
  saveFileNodeState(iNodeFile);
  // saveRepoFile(repoFile); Supposed to save the repertories in a file.
}
