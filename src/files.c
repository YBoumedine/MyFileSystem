#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include"files.h"
#include"nodes.h"
#include "blocks.h"


void creation_fichier(char *pathName, char *fileContent){
  int i, p = 0, lastP = 0, c = 0, ret =  strlen(fileContent)/16, wrote;
  bool done = false;
  char *strBytes;
  struct iNode *myFile;


  myFile = malloc(sizeof(struct iNode));
  myFile->pathName = malloc (strlen(pathName));
  strcpy(myFile->pathName, pathName);

  for(i=0; i<16; i++){
    myFile->blocksNum[i]= -1;
  }

    while(lastP < strlen(fileContent)){
       if(ret>0){
            strBytes = malloc(16);
            for(p = lastP; p<(lastP+16); p++){
              if(c<16){
                strBytes[c]= fileContent[p];
                c++;
              }
            }
          wrote = writeInBlock(strBytes);
        c = 0;
      }

      if(ret == 0 && strlen(fileContent)%16 >0){
        lastP = strlen(fileContent)-strlen(fileContent)%16;
        c = 0;

          strBytes = malloc((strlen(fileContent)%16+1));

          ret = strlen(fileContent)%16;
          for(p=lastP; p<strlen(fileContent); p++){
            if(c<ret){
              strBytes[c]= fileContent[p];
              c++;
            }
          }
          strBytes[c] = '\0';

          wrote = writeInBlock(strBytes);
          done = true;
      }

      for(i=0; i<16; i++){
        if(myFile->blocksNum[i] == -1){
            myFile->blocksNum[i] = wrote;
            break;
        }
      }

      if(ret == 0 && done){
        break;
      }

      lastP = p;
      ret--;

      memset(strBytes,'\0', 16);
      free(strBytes);
    }
    saveNodesInArray(myFile);
}
