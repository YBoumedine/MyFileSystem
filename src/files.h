#ifndef FILES_H
#define FILES_H

/**
 *
 * This Function is use to create files.
 *
 * @param filePath        the address of the file
 * @param fileContent     the content of the file
 */
void creation_fichier(char *filePath, char* fileContent);

#endif
