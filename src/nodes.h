#ifndef NODES_H
#define NODES_H

struct iNode {
  char *pathName;
  int blocksNum[16];
};

/*
 * Function that prints all the nodes.
 *
 *
 */
void printNode(struct iNode *node);

/*
 * Function that load nodes in parameter.
 *
 * @param iNodeFile         name of the pointer directed at the file
 */
void loadNodes(FILE *iNodeFile);

/*
 * This function save the nodes in a array.
 *
 * @param fileNode          Node that will be saved in the array
 */
void saveNodesInArray(struct iNode *fileNode);

/*
 * Save the state of the file.
 *
 * @param iNodeFile       name of the pointer directed at the file
 */
void saveFileNodeState(FILE *iNodeFile);

/*
 * This function is use to write in the nodes.
 *
 * @param node            Node that will be written inside the file
 *
 * @param iNodeFile       name of the pointer directed at the file
 */
void writeNode(struct iNode *node, FILE *iNodeFile);

/*
 * Search for the file with the pathName.
 *
 * @param pathName      the address of the file
 */
int searchNode(char *pathName);

/*
 * This function reads whats in the file.
 *
 * @param pathName      the address of the file
 */
void readNode(char *pathName);

/*
 * This function is use to delete a node.
 *
 * @param pathName      the address of the file
 */
void deleteNode(char *pathName);


#endif
